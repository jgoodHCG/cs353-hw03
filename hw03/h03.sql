-- File: companyDML-a-hwk
-- SQL/DML HOMEWORK (on the COMPANY database)
/*
Every query is worth 2 point. There is no partial credit for a
partially working query - think of this hwk as a large program and each query is a small part of the program.
--
IMPORTANT SPECIFICATIONS
--
(A)
-- Download the script file company.sql and use it to create your COMPANY database.
-- Dowlnoad the file companyDBinstance; it is provided for your convenience when checking the results of your queries.
(B)
Implement the queries below by ***editing this file*** to include
your name and your SQL code in the indicated places.   
--
(C)
IMPORTANT:
-- Don't use views
-- Don't use inline queries in the FROM clause - see our class notes.
--
(D)
After you have written the SQL code in the appropriate places:
** Run this file (from the command line in sqlplus).
** Print the resulting spooled file (companyDML-a.out) and submit the printout in class on the due date.
--
**** Note: you can use Apex to develop the individual queries. However, you ***MUST*** run this file from the command line as just explained above and then submit a printout of the spooled file. Submitting a printout of the webpage resulting from Apex will *NOT* be accepted.
--
*/
-- Please don't remove the SET ECHO command below.
SPOOL companyDML-a.out
SET ECHO ON
-- ---------------------------------------------------------------
-- 
-- Name: < ***** Justin Good, Rodney Fulk, Zach Cornell, Mike knooiheuisen, Nolan Locke ***** >
--
-- ------------------------------------------------------------
-- NULL AND SUBSTRINGS -------------------------------
--
/*(10A)
Find the ssn and last name of every employee who doesn't have a  supervisor, or his last name contains at least two occurences of the letter 'a'. Sort the results by ssn.
*/
-- <<< Your SQL code goes here >>>
SELECT E.Ssn, E.Lname
FROM EMPLOYEE E
WHERE E.Super_ssn IS NULL OR E.Lname LIKE '%a%a%'
ORDER BY E.Ssn;
--
-- JOINING 3 TABLES ------------------------------
-- 
/*(11A)
For every employee who works more than 30 hours on any project: Find the ssn, lname, project number, project name, and numer of hours. Sort the results by ssn.
*/
-- <<< Your SQL code goes here >>>
--
SELECT E.Ssn, E.Lname, P.Pnumber, P.Pname, W.Hours
FROM EMPLOYEE E, WORKS_ON W, PROJECT P
WHERE W.Pno = P.Pnumber AND E.Ssn = W.Essn AND W.Hours > 30
ORDER BY E.Ssn;

-- JOINING 3 TABLES ---------------------------
--
/*(12A)
Write a query that consists of one block only.
For every employee who works on a project that is not controlled by the department he works for:
 Find the employee's lname, the department he works for, the project number that he works on, 
 and the number of the department that controls that project. Sort the results by lname.
*/
-- <<< Your SQL code goes here >>>
SELECT E.Lname, D.Dname, W.Pno, P.Dnum
FROM EMPLOYEE E, WORKS_ON W, PROJECT P, DEPARTMENT D
WHERE E.Ssn = W.Essn AND W.Pno = P.Pnumber AND E.Dno = D.Dnumber AND E.Dno <> P.Dnum
ORDER BY E.Lname;
--
-- JOINING 4 TABLES -------------------------
--
/*(13A)
For every employee who works for more than 20 hours on any project that is located in the same
 location as his department: Find the ssn, lname, project number, project location, department number,
 and department location.Sort the results by lname
*/
-- <<< Your SQL code goes here >>>
SELECT E.Ssn, E.Lname, W.Pno, P.Plocation, P.Dnum, DL.Dlocation
FROM EMPLOYEE E, WORKS_ON W, PROJECT P, DEPT_LOCATIONS DL
WHERE E.Ssn = W.Essn AND W.Pno = P.Pnumber AND P.Dnum = DL.Dnumber AND W.Hours > 20 AND P.Plocation = DL.Dlocation
ORDER BY E.Lname;
--
-- SELF JOIN -------------------------------------------
-- 
/*(14A)
Write a query that consists of one block only.
For every employee whose salary is less than 70% of his immediate supervisor's salary: 
Find his ssn, lname, salary; and his supervisor's ssn, lname, and salary. Sort the results by ssn.  
*/
-- <<< Your SQL code goes here >>>
SELECT E.Ssn, E.Lname, E.Salary, E2.Ssn, E2.Lname, E2.Salary
FROM Employee E, Employee E2
WHERE E.Super_Ssn = E2.Ssn AND E.Salary < (E2.Salary * .70)
ORDER BY E.Ssn;
--
-- USING MORE THAN ONE RANGE VARIABLE ON ONE TABLE -------------------
--
/*(15A)
For projects located in Houston: Find pairs of last names such that the two employees
 in the pair work on the same project. Remove duplicates. Sort the result by the lname 
 in the left column in the result. 
*/
-- <<< Your SQL code goes here >>>  
SELECT E1.Lname, E2.Lname
FROM Employee E1, Employee E2, WORKS_ON W1, WORKS_ON W2, PROJECT P
WHERE E1.Ssn = W1.Essn AND
			E2.Ssn = W2.Essn AND
			W1.Pno = W2.Pno AND
			W1.Pno = P.Pnumber AND
			P.Plocation = 'Houston' AND 
			E1.Ssn > E2.Ssn		
ORDER BY E1.Lname;
			

------------------------------------
--
/*(16A) Hint: A NULL in the hours column should be considered as zero hours.
Find the ssn, lname, and the total number of hours worked on projects for every employee 
whose total is less than 40 hours. Sort the result by lname
*/ 
-- <<< Your SQL code goes here >>>
SELECT E.Ssn, E.Lname, SUM (W.Hours)
FROM EMPLOYEE E, WORKS_ON W 
WHERE E.Ssn = W.Essn 
GROUP BY E.Ssn, E.Lname
HAVING SUM (W.Hours) < 40
ORDER BY E.Lname;
--
------------------------------------
-- 
/*(17A)
For every project that has more than 2 employees working on it: Find the project number,
 project name, number of employees working on it, and the total number of hours worked by all employees
 on that project. Sort the results by project number.
*/ 
-- <<< Your SQL code goes here >>>
SELECT W.Pno, P.Pname, COUNT (W.Essn), SUM (W.Hours)
FROM WORKS_ON W, PROJECT P
WHERE W.Pno = P.Pnumber 
GROUP BY W.Pno, P.Pname
HAVING COUNT (W.Essn) > 2
ORDER BY W.Pno;
-- 
-- CORRELATED SUBQUERY --------------------------------
--
/*(18A)
For every employee who has the highest salary in his department: Find the dno, ssn, lname, and salary . 
Sort the results by department number.
*/
-- <<< Your SQL code goes here >>>

SELECT E.Dno, E.Ssn, E.Lname, E.Salary
FROM EMPLOYEE E
WHERE E.Salary = (SELECT MAX(E1.Salary) 
					FROM EMPLOYEE E1 
					WHERE E.Dno = E1.Dno)
GROUP BY E.Dno, E.Ssn, E.Lname, E.Salary
ORDER BY E.Dno; 

--
-- NON-CORRELATED SUBQUERY -------------------------------
--
/*(19A)
For every employee who does not work on any project that is located in Houston: 
Find the ssn and lname. Sort the results by  
*/
-- <<< Your SQL code goes here >>>

SELECT DISTINCT E.Ssn, E.Lname
FROM EMPLOYEE E, WORKS_ON W, PROJECT P
WHERE E.Ssn = W.Essn AND W.Pno IN (SELECT P.Pnumber 
									FROM PROJECT P
									WHERE (P.Plocation != 'Houston'))
GROUP BY E.Ssn, E.Lname
ORDER BY E.Lname;

--
-- DIVISION ---------------------------------------------
--
/*(20A) Hint: This is a DIVISION query
For every employee who works on every project that is located in Stafford: Find the ssn and lname.
 Sort the results by lname
*/
-- <<< Your SQL code goes here >>>
SELECT DISTINCT E.Ssn, E.Lname
FROM EMPLOYEE E
WHERE EXISTS ({SELECT P.Pnumber 
					FROM PROJECT P, WORKS_ON W
					WHERE E.Ssn = W.Essn AND W.Pno = P.Pnumber)
					MINUS 
					(SELECT P.Pnumber 
					FROM PROJECT P
					WHERE P.Plocation = 'Stafford'))
GROUP BY E.Ssn, E.Lname
ORDER BY E.Lname 

--
SET ECHO OFF
SPOOL OFF


