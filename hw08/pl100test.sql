@_drop
@_create
@ pl100.sql
--
SET ECHO ON
-- Testing the trigger for INSERT, and then restoring the database
SELECT * FROM BOATS;
INSERT INTO boats VALUES (120, 'titanic', 'white', 350, 32, 47);
--
INSERT INTO boats VALUES (120, 'titanic', 'white', 350, 32, 32);
-- Inspect then restore the Boats table
SELECT * FROM BOATS;
DELETE FROM Boats WHERE bid=120;
SELECT * FROM BOATS;
--
-- Testing the trigger for UPDATE, and then restoring the database
UPDATE Boats SET logkeeper=55 WHERE bid=101;
--
UPDATE Boats SET logkeeper=29 WHERE bid=101;
-- Inspect then restore the Boats table
SELECT * FROM BOATS;
UPDATE Boats SET logkeeper=95 WHERE bid=101;
SELECT * FROM BOATS;
--
DROP TRIGGER bIC5_TA;
SET ECHO OFF