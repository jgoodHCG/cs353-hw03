-- PL100
-- author: JRA
--
CREATE OR REPLACE TRIGGER bIC5_TA
BEFORE INSERT OR UPDATE OF logKeeper ON Boats
FOR EACH ROW
DECLARE
numFound INTEGER;
BEGIN
SELECT COUNT(*)
INTO numFound
FROM Sailors S WHERE S.trainee = :NEW.logKeeper;
IF numFound < 1
THEN
RAISE_APPLICATION_ERROR(-20001, '+++++INSERT or UPDATE rejected. '||
'logKeeper '||:NEW.logkeeper|| ' is not a trainee');
END IF;
END;
/
SHOW ERROR