-- File: PLh110.sql
-- Author: <<< ENTER YOUR NAME HERE >>>
--
CREATE OR REPLACE TRIGGER bIC5_TB
BEFORE DELETE OR UPDATE OF trainee ON Sailors
FOR EACH ROW
DECLARE
numTrainers INTEGER;
PRAGMA AUTONOMOUS_TRANSACTION;
BEGIN
SELECT COUNT(*)
INTO numTrainers
FROM Boats B WHERE B.logKeeper = :OLD.trainee;
IF numTrainers > 0 
THEN 
RAISE_APPLICATION_ERROR(-20001, '+++++ DELETE or UPDATE rejected. Trainee..'||
       :OLD.trainee||'.. is a LogKeeper');
END IF;
END;
/
SHOW ERROR