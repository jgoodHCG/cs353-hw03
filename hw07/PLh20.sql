-- File PLh20.sql
-- Author: Rodney Fulk
-------------------------------------------------------------------
SET SERVEROUTPUT ON
SET VERIFY OFF
------------------------------------
ACCEPT rateDecrement NUMBER PROMPT 'Enter the rate decrement: '
ACCEPT allowedMinRate NUMBER PROMPT 'Enter the allowed min. rate: '
DECLARE
br boats%ROWTYPE;
newRate SIMPLE_INTEGER := 0;
CURSOR bCursor IS
SELECT B.*
FROM reservations R, boats B
WHERE R.bid <> B.bid 
MINUS
SELECT B.*
FROM reservations R, boats B
WHERE R.bid = B.bid;
BEGIN
OPEN bCursor;
LOOP
---- Fetch the qualifying rows one by one
FETCH bCursor INTO br;
EXIT WHEN bCursor%NOTFOUND;
-- Print the sailor' old record
DBMS_OUTPUT.PUT_LINE ('+++++ boat: '||br.bid||': old rate = '||br.rate);
DECLARE
belowAllowedMin EXCEPTION;
BEGIN
newRate := br.rate - &rateDecrement;
IF newRate < &allowedMinRate
THEN RAISE belowAllowedMin;
ELSE UPDATE boats
SET rate = newRate
WHERE boats.bid = br.bid;
DBMS_OUTPUT.PUT_LINE ('----- boat: '||br.bid||': new rate = '||newRate);
END IF;
EXCEPTION
WHEN belowAllowedMin THEN
DBMS_OUTPUT.PUT_LINE('----- Update rejected: '||
'The new rating would have been: '|| newRate);
WHEN OTHERS THEN
DBMS_OUTPUT.PUT_LINE('+++++ update rejected: ' ||
SQLCODE||'...'||SQLERRM);
END;
END LOOP;
COMMIT;
CLOSE bCursor;
END;
/