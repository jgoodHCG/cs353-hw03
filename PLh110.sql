-- --------------------------------------
-- File: PLh110.sql
-- Author: Justin Good
--
CREATE OR REPLACE TRIGGER bIC5_TB
BEFORE DELETE OR UPDATE OF trainee ON Sailors
FOR EACH ROW
DECLARE
    PRAGMA AUTONOMOUS_TRANSACTION; -- You'll need this directive
-- 
BEGIN
    SELECT COUNT(*)
    INTO tNum
    FROM Boats B 
    WHERE B.logkeeper = :OLD.trainee;
    IF tNum > 0
    THEN
    RAISE_APPLICATION_ERROR(-20001, '+++++ DELETE or UPDATE rejected. Trainee..'||:OLD.trainee||'.. is a LogKeeper');
    END IF;
--
END;
/
SHOW ERROR
-- -------------------------------------

